package com.firebaseio.hackernews.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.firebaseio.hackernews.DetailActivity;
import com.firebaseio.hackernews.R;
import com.firebaseio.hackernews.apis.Api;
import com.firebaseio.hackernews.apis.Callbacks;
import com.firebaseio.hackernews.models.Story;
import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class StoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_FULL = 0;
    private static final int TYPE_HALF = 1;
    public static final int REQUEST = 99;

    String titleFavourite;

    public class StoryViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.stories_title)
        TextView title;

        @BindView(R.id.stories_comment_count)
        TextView commentCount;

        @BindView(R.id.stories_score)
        TextView score;

        public StoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class FavouriteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.stories_title)
        TextView title;

        public FavouriteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private List<Long> stories;
    private Context context;

    public StoryAdapter(List<Long> stories) {
        this.stories = stories;
    }

    @Override
    public int getItemCount() {
        return stories.size();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView;
        RecyclerView.ViewHolder holder = null;

        switch (viewType) {
            case TYPE_FULL:
                itemView = inflater.inflate(R.layout.row_favourite, parent, false);
                holder = new FavouriteViewHolder(itemView);
                break;
            case TYPE_HALF:
                itemView = inflater.inflate(R.layout.row_stories, parent, false);
                holder = new StoryViewHolder(itemView);
                break;
        }

        return holder;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_FULL;
        } else {
            return TYPE_HALF;
        }
    }

    public Long getItem(int position) {
        return stories.get(position - 1);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position == 0) {
            if (titleFavourite != null) {
                ((FavouriteViewHolder) holder).title.setText("Favourite : " +titleFavourite);
            } else {
                ((FavouriteViewHolder) holder).title.setText("Favourite not exist");
            }
        } else {
            final StoryViewHolder viewHolder = ((StoryViewHolder) holder);
            final Long id = getItem(position);
            new Api().getStory(id.toString(), new Callbacks.Stories() {
                @Override
                public void onSucccess(@NonNull Story result) {
                    viewHolder.title.setText(result.getTitle());
                    viewHolder.commentCount.setText("Comments : " + result.getDescendants());
                    viewHolder.score.setText("Score : " + result.getScore());

                    final String results = new Gson().toJson(result, Story.class);
                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(context, DetailActivity.class);
                            intent.putExtra("DATA", results);
                            ((AppCompatActivity) context).startActivityForResult(intent, REQUEST);
                        }
                    });
                }

                @Override
                public void onFailure(@NonNull Throwable throwable) {
                    throwable.printStackTrace();
                }

                @Override
                public void onError(@NonNull String message) {
                    Toast.makeText(context, message, Toast.LENGTH_LONG).show();
                }
            });

        }
    }


    public void addData(Long[] data) {
        stories = Arrays.asList(data);
    }

    public void changeFavourite(String title) {
        titleFavourite = title;
    }
}
