package com.firebaseio.hackernews.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebaseio.hackernews.R;
import com.firebaseio.hackernews.apis.Api;
import com.firebaseio.hackernews.apis.Callbacks;
import com.firebaseio.hackernews.models.Comment;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.comment)
        TextView comment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private List<Long> comments;

    public CommentAdapter(List<Long> comments) {
        this.comments = comments;
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.row_comment, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Long id = getItem(position);
        new Api().getComment(id.toString(), new Callbacks.Comments() {
            @Override
            public void onSucccess(@NonNull Comment comment) {
                holder.comment.setText(Html.fromHtml(comment.getText()));
            }

            @Override
            public void onFailure(@NonNull Throwable throwable) {

            }

            @Override
            public void onError(@NonNull String message) {

            }
        });
    }

    public Long getItem(int position) {
        return comments.get(position);
    }

    public void addData(List<Long> data) {
        comments.addAll(data);
    }

}
