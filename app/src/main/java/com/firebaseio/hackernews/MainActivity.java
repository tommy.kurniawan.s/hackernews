package com.firebaseio.hackernews;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.firebaseio.hackernews.adapters.StoryAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    private StoryAdapter adapter;
    private List<Long> stories = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Top Stories");

        adapter = new StoryAdapter(stories);
        Controller controller = new Controller();
        controller.showTopStories(this, recyclerView, adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == StoryAdapter.REQUEST) {
            if (resultCode == Activity.RESULT_OK) {
                adapter.changeFavourite(data.getStringExtra("DATA"));
                adapter.notifyDataSetChanged();
            }
        }
    }
}
