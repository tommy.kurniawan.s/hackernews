package com.firebaseio.hackernews;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebaseio.hackernews.adapters.CommentAdapter;
import com.firebaseio.hackernews.adapters.StoryAdapter;
import com.firebaseio.hackernews.apis.Api;
import com.firebaseio.hackernews.apis.Callbacks;
import com.firebaseio.hackernews.models.Comment;
import com.firebaseio.hackernews.models.Story;
import com.google.gson.Gson;

import java.util.Date;

public class Controller {
    private ProgressDialog progressDialog;
    private Story favouritedStory;

    public void showTopStories(final MainActivity activity, RecyclerView recyclerView, final StoryAdapter adapter) {

        GridLayoutManager layoutManager = new GridLayoutManager(activity, 2);

        // Create a custom SpanSizeLookup where the first item spans both columns
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? 2 : 1;
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        showProgress(activity);
        new Api().getTopStories(new Callbacks.TopStories() {
            @Override
            public void onSucccess(@NonNull Long[] result) {
                adapter.addData(result);
                adapter.notifyDataSetChanged();
                hideProgress();
            }

            @Override
            public void onFailure(@NonNull Throwable throwable) {
                throwable.printStackTrace();
                hideProgress();
            }

            @Override
            public void onError(@NonNull String message) {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
                hideProgress();
            }
        });
    }

    public void showDetailStory(final DetailActivity activity, RecyclerView recyclerView, TextView title, TextView author, TextView date, ImageView favourite, TextView desc, final CommentAdapter adapter) {
        Bundle bundle = activity.getIntent().getExtras();
        Story story = null;
        if (bundle != null) {
            story = new Gson().fromJson(bundle.getString("DATA"), Story.class);
            favouritedStory = story;
            title.setText(story.getTitle());
            author.setText(story.getBy());
            date.setText(DateFormat.format("dd/MM/yyyy", new Date(story.getTime())).toString());
            desc.setText(story.getUrl());
            if (story.getKids() != null) {
                adapter.addData(story.getKids());
            }
        }

        LinearLayoutManager layoutManager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));

        favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("DATA", favouritedStory.getTitle());
                activity.setResult(Activity.RESULT_OK, intent);
                Toast.makeText(activity, "Favourited", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showProgress(Context context) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Loading");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
    }

    private void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
