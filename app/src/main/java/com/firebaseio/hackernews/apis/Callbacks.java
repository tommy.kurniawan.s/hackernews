package com.firebaseio.hackernews.apis;

import android.support.annotation.NonNull;

import com.firebaseio.hackernews.models.Comment;
import com.firebaseio.hackernews.models.Story;

import org.json.JSONArray;

public interface Callbacks {

    void onFailure(@NonNull Throwable throwable);

    void onError(@NonNull String message);

    interface TopStories extends Callbacks {
        void onSucccess(@NonNull Long [] result);
    }

    interface Stories extends Callbacks {
        void onSucccess(@NonNull Story result);
    }

    interface Comments extends Callbacks {
        void onSucccess(@NonNull Comment cooment);
    }
}
