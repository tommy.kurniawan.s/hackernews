package com.firebaseio.hackernews.apis;

import com.firebaseio.hackernews.models.Comment;
import com.firebaseio.hackernews.models.Story;
import com.firebaseio.hackernews.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET(Constants.URL_TOPSORIES)
    Observable<Long[]> getTopStories();

    @GET(Constants.SERVICE_ITEM + "{id}.json")
    Observable<Story> getStory(@Path("id") String storyId);

    @GET(Constants.SERVICE_ITEM + "{id}.json")
    Observable<Comment> getComment(@Path("id") String commentId);

    class Factory {
        private static ApiInterface service;

        public static ApiInterface getInstance() {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

            httpClient.addInterceptor(logging);

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .setDateFormat("")
                    .create();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.HOST)
                    .client(new OkHttpClient.Builder()
                            .connectTimeout(90, TimeUnit.SECONDS)
                            .readTimeout(90, TimeUnit.SECONDS)
                            .writeTimeout(90, TimeUnit.SECONDS)
                            .build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            service = retrofit.create(ApiInterface.class);
            return service;
        }

    }
}
