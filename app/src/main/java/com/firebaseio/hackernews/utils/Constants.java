package com.firebaseio.hackernews.utils;

public class Constants {

    public static final String HOST = "https://hacker-news.firebaseio.com/";
    public static final String VER = "v0/";

    public static final String SERVICE_ITEM = VER + "item/";

    public static final String URL_TOPSORIES = VER + "topstories.json";
}
