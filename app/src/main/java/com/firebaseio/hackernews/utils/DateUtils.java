package com.firebaseio.hackernews.utils;

import android.text.format.DateFormat;

import java.util.Date;

public class DateUtils {

    public static String longToDate(Long timestamp) {
        String dateString = DateFormat.format("dd/MM/yyyy", new Date(timestamp)).toString();
        return dateString;
    }
}
