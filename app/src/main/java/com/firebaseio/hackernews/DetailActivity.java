package com.firebaseio.hackernews;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebaseio.hackernews.adapters.CommentAdapter;
import com.firebaseio.hackernews.models.Comment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.story_title)
    TextView title;

    @BindView(R.id.story_author)
    TextView author;

    @BindView(R.id.story_date)
    TextView date;

    @BindView(R.id.story_favourite)
    ImageView favourite;

    @BindView(R.id.story_desc)
    TextView desc;

    private List<Long> comments = new ArrayList<>();
    private CommentAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("Story Detail");

        adapter = new CommentAdapter(comments);
        Controller controller = new Controller();
        controller.showDetailStory(this, recyclerView, title, author, date, favourite, desc, adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
